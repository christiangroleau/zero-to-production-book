This project is to build a todo app that stores and allows editing of
a user's todos. Therefore the frontend needs to provide interfaces to

 - Register a new account, including confirming the email
 - Login and logout, and changing the password
 - Manage TODOs (creating, editing, completing, and deleting)

ideally as a Single Page App (for a better user experience) and
provide the ability to become a progressive web app.

## Tech choices

![React logo](img/react.png){: style="height: 80px;float: right"}

To do this I've chosen to use [React](https://reactjs.org/) as the
base framework with various libraries to add additional
functionality.

First lets setup a couple of small configuration options that will
make the following pages easier.

## Proxying requests

In development we will want api requests to be proxied to the backend,
which we can simply do by adding to `frontend/package.json`,

```json
{
  ...,
  "proxy": "http://localhost:5000"
}
```

## Absolute imports

It is much easier to write imports relative to the `src` directory,
which I'll term absolute, than to write relative imports
i.e. `components/Component` is eaiser than
`../../components/Component` especially when refactoring code.

To achieve this the `frontend/tsconfig.json` file should be updated to
include,

```json
{
  ...
  "compilerOptions": {
    "baseUrl": "./",
    ...
  },
}
```

where `...` represnts the existing values.

## Globally unique ids

We will need to assign unique id values to dom elements, often to
improve accessibility, which we can do using
[React-UID](https://github.com/theArnica/react-uid).

React-UID is installed via npm,

```shell
npm install --save react-uid
```

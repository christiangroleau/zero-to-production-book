![date-fns logo](img/date-fns.png){: style="height: 80px;float: right"}

As with the [backend](../backend/models.md) it helps to use models
representing the data used. These models help the linters ensure that
we are using the data correctly and that the correct types are
used. We can also use the model to correctly convert to and from the
JSON representation used to communicate with the backend API.

As JSON has no date type we use ISO 8601 formatted strings, see the
[backend validation](../backend/validation.md). To turn these strings
into javascript `Date` instances we'll use
[date-fns](https://date-fns.org/). As it provides all the functionality
we'll need using the javascript `Date` type.

date-fns is installed via npm,

!!! info inline end ""

    Run this command in `frontend/`

```shell
npm install --save date-fns
```

We need a model for `Todo`s that can be constructed from a JSON
associate array or from user entered form data and then converted back
to json when sent to the backend API. The former is achieved by
converting the `due` argument as required into a `Date`, the latter is
achieved via a `toJSON` method that is automatically called when
converting to JSON. The following should be added to
`frontend/src/models.ts`,

```typescript
import { formatISO, parseISO } from "date-fns";

interface ITodoParams {
  complete: boolean;
  due: Date | string | null;
  id: number;
  task: string;
}

export class Todo {
  complete: boolean;
  due: Date | null;
  id: number;
  task: string;

  constructor({ complete, due, id, task }: ITodoParams) {
    this.complete = complete;
    if (due instanceof Date) {
      this.due = due;
    } else if (due !== null) {
      this.due = parseISO(due);
    } else {
      this.due = due;
    }
    this.id = id;
    this.task = task;
  }

  toJSON(): any {
    return {
      complete: this.complete,
      due:
        this.due !== null
          ? formatISO(this.due, { representation: "date" })
          : null,
      id: this.id,
      task: this.task,
    };
  }
}
```

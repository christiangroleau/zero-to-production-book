![Axio logo](img/axios.png){: style="height: 80px;float: right"}

We will need to make requests from the frontend to the backend to
retrieve the todo data and to carry out any action related to them. To
do this we'll use [axios](https://axios-http.com) as it has a nice API
for sending and receiving JSON. Axios is installed via npm,

!!! info inline end ""

    Run this command in `frontend/`

```shell
npm install --save axios
```

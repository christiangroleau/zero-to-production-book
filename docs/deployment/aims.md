This project is to build a Todo app that users can use anywhere via a
browser. Therefore the deployment needs to be accessible via the
internet, specifically via a domain name. In addition we want to spend
the minimal possible effort maintaining and deploying the app.

## Provider choices

I've used [Google Cloud](https://cloud.google.com/),
[AWS](https://aws.amazon.com/), and [Heroku](https://www.heroku.com/)
to host applications in the past. Heroku is the easiest to get started
with and therefore it is the best choice for this book. However we'll
only use Heroku products that are available on the other clouds, for
example we'll containerise the app rather than using Heroku's python
deployment.

The domain name is the identity of the website, control over it is
crucial to prove ownership and to setup the hosting. For these reasons
I prefer to use a different provider to manage the domain name alone
and I trust [Gandi](https://www.gandi.net/) to do so.

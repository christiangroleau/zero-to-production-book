In production we will use [Postmark](../backend/email.md) to send
emails to the users. To do this we'll need to add DNS records to prove
to Postmark that we control the domain, and can therefore send emails
from it. The values are available from your postmark account and are
added to `infrastructure/dns.tf`,

```terraform
resource "gandi_livedns_record" "tozo_dev_DKIM" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "20210807103031pm._domainkey"
  type   = "TXT"
  ttl    = 10800
  values = ["k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDm2rt5ln+UTNO+QzCyGVdbXp94n2LJkr8UJEjAs599g0kaSjZ/jzfIXASdrCpOwOOMTwp4d1pBwlCmop2p1PDrSAgQCrwOoTWI3DQPn+YXOt7vXAVQnl+WKoZvEnUc3vaOBxqgenPL6gLAp91B0j0Jvwh+9scZme+IjGLszwO+DwIDAQAB"]
}

resource "gandi_livedns_record" "tozo_dev_CNAME" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "pm-bounces"
  type   = "CNAME"
  ttl    = 10800
  values = ["pm.mtasv.net."]
}
```

!!! note

    The values used above are for my Postmark account, you will have
    different values.

In addition we need to make the Postmark API token available in the
environment during the runtime. This is done by adding the token to
your secrets file and then adding the following to
`infrastructure/heroku.tf`,

```terraform
variable "postmark_token" {
  sensitive = true
}

resource "heroku_app" "tozo" {
  ...

  sensitive_config_vars = {
    POSTMARK_TOKEN = var.postmark_token
  }
}
```

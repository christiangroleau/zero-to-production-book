We'll want a memorable domain name for users to find and identify our
app. I've gone for `tozo.dev` which I've purchased from Gandi.

Now we have a domain name we need visitors to it to find our app
running on Heroku. This is where DNS comes in, DNS allows the visitor
to lookup our domain name and find the location it is served
from. We'll need to add the correct root DNS record so that it points
at our Heroku app.

To do so we should first delete any DNS records already assigned in
Gandi - this is something that frustratingly we'll have to do
manually. However after this we can use the Gandi Terraform provider,
after activating it by adding the following to infrastructure/main.tf,

```terraform
terraform {
  required_providers {
    ...
    gandi = {
      version = "2.0.0-rc2-fork-3"
      source  = "manvalls/gandi"
    }
  }
}
```

and running `terraform init` to initialise it.

Next we should retreive an api key from Gand, and then add it to
`infrastructure/secrets.auto.tfvars`,

```terraform
gandi_api_key = "abcd"
```

Which we can use to configure the provider by adding the following to
`infrastructure/dns.tf`,

```terraform
variable "gandi_api_key" {
  sensitive = true
}

provider "gandi" {
  key = var.gandi_api_key
}
```

As we only need to configure the root record as an ALIAS to the Heroku
app CNAME we can add the following to `infrastructure/dns.tf`,

```terraform
data "gandi_domain" "tozo_dev" {
  name = "tozo.dev"
}

resource "gandi_livedns_record" "tozo_dev_ALIAS" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "@"
  type   = "ALIAS"
  ttl    = 3600
  values = [heroku_domain.tozo.cname]
}
```

and the following to `infrastructure/heroku.tf` to ensure the Heroku
app responds to the `tozo.dev` domain,

```terraform
resource "heroku_domain" "tozo" {
  app      = heroku_app.tozo.name
  hostname = "tozo.dev"
}
```

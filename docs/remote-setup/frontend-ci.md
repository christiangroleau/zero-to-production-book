As a [reminder](../local-setup/frontend-tooling.md) the commands to
check the formatting, lint, and test are,

```shell
npm run format
npm run lint
npm run test
```

In addition it is helpful to try to build the frontend as part of the
CI, so as to ensure it can be built with the changes. This can be
done via,

```shell
npm run build
```

To have these run as part of CI i.e. for every change to the remote
repository the `.gitlab-ci.yml` file placed in the root of the
repository should contain,

```yaml
frontend-ci:
  image: node:15-alpine

  cache:
    key: node-cache
    paths:
      - frontend/node_modules/
      - frontend/.npm/

  before_script:
  - cd frontend
  - npm ci --cache .npm --prefer-offline

  script:
  - npm run format
  - npm run lint
  - npm run test
  - npm run build

  only:
    changes:
      - frontend/**/*
      - .gitlab-ci.yml
```

## Security checks

It is good practice to regularly update any dependencies used, in
order to get the latest security fixes and updates. Alongside this it
helps to regularly check if there are known security issues, which can
be done via the audit command,

```shell
npm audit
```

This can be run periodically using a Gitlab-CI schedule, by adding the
following to the `.gitlab-ci.yml` file placed in the root of the
repository,

```yaml
audit-check:
  image: node:15-alpine

  cache:
    key: node-cache
    paths:
      - frontend/node_modules/
      - frontend/.npm/

  before_script:
  - cd frontend
  - npm ci --cache .npm --prefer-offline

  script:
  - npm audit

  only:
  - schedules
```

which will run on the schedule defined in the [ci
setup](../remote-setup/ci.md).

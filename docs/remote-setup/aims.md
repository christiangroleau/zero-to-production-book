The aim of this chapter is to setup the remote tooling to continuously
ensure that the main branch has no detectable errors and can therefore
be deployed into production at any time, and to allow multiple
developers to collabortate.

## CI

Continuous Integration, CI, is the process of continually and quickly
integrating changes to the main branch. To achieve this changes must
be tested to ensure that they can be safely integrated, hence CI is
often synonymous with linting and testing.

## Dependency checking

Dependency checking is checking that the versions of the dependencies
installed have no known (reported) security issues. This is something
that I check in CI with the build failing, and hence requiring fixing
if there are known issues.

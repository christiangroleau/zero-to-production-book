Configuration allows the app to run in multiple environments without
having to alter the code. For example it makes sense to enable error
tracking in production, but to disable it in local development (where
errors are tracked in the console). It also allows secrets to be
managed separately from the code, and hence more securely.

I like to use environment variables to configure the app, with
different values per environment. To load the environment
[python-dotenv](https://github.com/theskumar/python-dotenv) can be
used, which is installed with poetry,

!!! info inline end ""

    Run this command in `backend/`

```shell
poetry add python-dotenv
```

We have four environments we need consider, development, testing, ci,
and production. Each will require different settings and so I manage
the environments via `backend/development.env`, `backend/test.env`,
`backend/ci.env`, and production environment variables - see [heroku
setup](../deployment/heroku.md) for the production setup.

These environments are then loaded in the `backend/scripts.py` by
adding the following,

```python
...

from dotenv import load_dotenv

def start() -> None:
    load_dotenv("development.env")
    ...

def test() -> None:
    load_dotenv("test.env")
    ...

def test_ci() -> None:
    load_dotenv("ci.env")
    ...
```

We can then utilise the environment variables in the app by loading
them into the app's config during initialisation,

```python
import os

from quart import Quart

def create_app() -> Quart:
    app = Quart(__name__)

    for key in ["BASE_URL", "SECRET_KEY"]:
        app.config[key] = os.environ[key]
    ...
```

with an initial configuration as given in the files,

=== "backend/development.env"

    ```ini
    BASE_URL="http://localhost:3000"
    SECRET_KEY="secret key"
    ```

=== "backend/test.env"

    ```ini
    BASE_URL="http://localhost:3000"
    SECRET_KEY="secret key"
    ```

=== "backend/ci.env"

    ```ini
    BASE_URL="http://localhost:3000"
    SECRET_KEY="secret key"
    ```

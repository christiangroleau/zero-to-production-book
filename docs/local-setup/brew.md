![Brew](img/brew.png){: style="height: 80px;float: right"}
![Linuxbrew](img/linuxbrew.png){: style="height:80px; float: right"}

I'm assuming you don't have tooling such as Python installed and so
the first step is to install a package manager than can install Python
and everything else you'll need. In my view Homebrew is the best
package manager for this and works on [MacOS,
brew.sh](https://brew.sh/), or [Linux,
linuxbrew.sh](http://linuxbrew.sh/). (I don't use Windows so I'm not
sure what to use).

!!! note

    A package manager is used to install packages, for example
    specific versions of Python or Node. This is useful as it is often
    not trivial to install these packages, except when a package
    manager does it for you.

With brew installed you can now install packages, e.g. to install
python

!!! info inline end ""

    Run this command in any location

```shell
brew install python
```

or to view the packages you have installed,

!!! info inline end ""

    Run this command in any location

```shell
brew list
```

From this point on I'll assumed you have brew installed and that you
are using it to manage you system packages.

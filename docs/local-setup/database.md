![Postgres logo](img/postgres.png){: style="height: 80px;float: right"}

The data used in the app will will need to be stored somewhere. The
relational database [postgres](https://www.postgresql.org/) is my go
to choice. We'll need it installed locally for development, which we
can do using brew,

!!! info inline end ""

    Run these commands in any location

```shell
brew install postgres
brew services start postgresql
```

You can then access the local database using the `psql` client,

!!! info inline end ""

    Run this command in any location

```shell
psql -U postgres
```

note that `postgres` is the default user and may not exist, if it
isn't create it using,

!!! info inline end ""

    Run this command in any location

```shell
createuser -s postgres
```

We'll use the `postgres` superuser to create a specific user for our
app later.

# Zero to Production Book

A guide to implementing a production ready [Quart](https://github.com/pallets/quart) application

## Reading this book online

The instructions presented in this excellent book may be found here:

https://pgjones.dev/tozo/


## Application

The application source code is here:

https://github.com/pgjones/tozo


## About this repo

This is a 'fork' of the now archived GitLab [repo](https://gitlab.com/pgjones/zero-to-production-book/)
